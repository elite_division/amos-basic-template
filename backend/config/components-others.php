<?php
/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */
return [
    'errorHandler' => [
        'errorAction' => 'error/error',
//        'errorAction' => 'layout/error/error', questa versione richiede layout: test/1.4.0
     ],
    'eventSequence' => [
        'class' => '\raoul2000\workflow\events\BasicEventSequence',
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
                'logVars' => ['_SERVER'],
            ],
        ],
    ],
    'request' => [
        'csrfParam' => '_csrf-backend',
        'enableCookieValidation' => false,
    ],
    'session' => [
        // this is the name of the session cookie used for login on the backend
        'name' => 'advanced-backend',
    ],
    'user' => [
        'class' => 'elitedivision\amos\core\user\AmosUser',
        'identityClass' => 'elitedivision\amos\core\user\User',
        'loginUrl' => '/admin/security/login',
        'enableAutoLogin' => true,
        'identityCookie' => [
            'name' => '_identity-backend',
            'httpOnly' => true,
            'secure' => true
        ],
    ],
    'socialShare' => [
        'class' => \elitedivision\amos\core\components\ConfiguratorSocialShare::class,
    ],
];
