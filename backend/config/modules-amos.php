<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

$modules = [
    'amministra-utenti' => [
        'class' => 'elitedivision\amos\admin\RoleManager',
        'layout' => "@vendor/elitedivision/amos-core/views/layouts/admin",
        //'left-menu', // it can be '@path/to/your/layout'.
        'controllerMap' => [
            'assignment' => [
                'class' => 'mdm\admin\controllers\AssignmentController',
                'userClassName' => 'common\models\User',
                'idField' => 'id'
            ],
        ],
        'menus' => [
            'assignment' => [
                'label' => 'Gestisci Assegnazioni' // TODO translate
            ],
        ]
    ],
    'comuni' => [
        'class' => 'elitedivision\amos\comuni\AmosComuni',
    ],
    'dashboard' => [
        'class' => 'elitedivision\amos\dashboard\AmosDashboard',
    ],
    'layout' => [
        'class' => 'elitedivision\amos\layout\Module',
    ],
    'myactivities' => [
         'class' => 'elitedivision\amos\myactivities\AmosMyActivities',
    ],
    'privileges' => [
	    'class' => 'elitedivision\amos\privileges\AmosPrivileges',
    ],
    'upload' => [
        'class' => 'elitedivision\amos\upload\AmosUpload',
    ],
    'utility' => [
        'class' => 'elitedivision\amos\utility\Module'
    ],
    'workflow' => [
       'class' => 'elitedivision\amos\workflow\AmosWorkflow',
   ],
];

return $modules;