<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
        'view' => [
            'class' => 'elitedivision\amos\core\components\AmosView',
        ],
];
