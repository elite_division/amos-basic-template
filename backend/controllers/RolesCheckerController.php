<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

namespace backend\controllers;


use yii\web\Controller;

class RolesCheckerController extends Controller
{

    public $layout = '@vendor/elitedivision/amos-core/views/layouts/main';

    public function actionIndex()
    {
        $Roles = \Yii::$app->getAuthManager()->getRoles();

        return $this->render('index', [
            'Roles' => $Roles
        ]);


    }

}