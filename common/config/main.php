<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */
use yii\helpers\ArrayHelper;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$modules = ArrayHelper::merge(
    require(__DIR__ . '/modules-others.php'),
    require(__DIR__ . '/modules-amos.php')
);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$bootstrap = ArrayHelper::merge(
    [],
    require(__DIR__ . '/bootstrap-extra.php')
);

//Installation file
if (file_exists(__DIR__ . '/install.php')) {
    $bootstrap = ArrayHelper::merge(
        include(__DIR__ . '/install.php'),
        $bootstrap
    );
}

if (isset($modules['chat'])) {
    $bootstrap[] = 'chat';
}
if (isset($modules['cwh'])) {
    $bootstrap[] = 'cwh';
}
if (isset($modules['tag'])) {
    $bootstrap[] = 'tag';
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$components = ArrayHelper::merge(
    require(__DIR__ . '/components-others.php'),
    require(__DIR__ . '/components-amos.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
return [
    'aliases' => [
        '@file' => dirname(__DIR__),
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'language' => 'it-IT',
    'timeZone' => 'Europe/Rome',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'Amos Basic Template',
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    'bootstrap' => $bootstrap,
    'components' => $components,
    'modules' => $modules,
    'params' => $params,
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
];
