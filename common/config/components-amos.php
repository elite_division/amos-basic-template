<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
    'formatter' => [
        'class' => 'elitedivision\amos\core\formatter\Formatter',
        'dateFormat' => 'php:d/m/Y',
        'datetimeFormat' => 'php:d/m/Y H:i',
        'timeFormat' => 'php:H:i',
        'defaultTimeZone' => 'Europe/Rome',
        'timeZone' => 'Europe/Rome',
        'locale' => 'it-IT',
        'thousandSeparator' => '.',
        'decimalSeparator' => ',',
    ],
    'imageUtility' => [
        'class' => 'elitedivision\amos\core\components\ImageUtility',
    ],
    'view' => [
         'class' => 'elitedivision\amos\core\components\AmosView',
    ],
    'workflowSource' => [
        'class' => 'elitedivision\amos\core\workflow\ContentDefaultWorkflowDbSource',
    ],
];
