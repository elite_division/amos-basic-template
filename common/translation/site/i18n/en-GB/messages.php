<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\comments\i18n\en-GB
 * @category   CategoryName
 */

return [

  'Cookie' => 'Cookie',
  'Privacy' => 'Privacy',
  'Trattamento dati personali' => 'Personal data processing',
];
