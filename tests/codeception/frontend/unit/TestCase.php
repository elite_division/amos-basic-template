<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace tests\codeception\frontend\unit;

/**
 * @inheritdoc
 */
class TestCase extends \yii\codeception\TestCase
{
    public $appConfig = '@tests/codeception/config/frontend/unit.php';
}
