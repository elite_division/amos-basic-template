<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace tests\codeception\frontend\_pages;

use yii\codeception\BasePage;

/**
 * Represents about page
 * @property \tests\codeception\frontend\AcceptanceTester|\tests\codeception\frontend\FunctionalTester $actor
 */
class AboutPage extends BasePage
{
    public $route = 'site/about';
}
