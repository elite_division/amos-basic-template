<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace tests\codeception\backend\unit;

class TestCase extends \yii\codeception\TestCase
{
    public $appConfig = '@tests/codeception/config/backend/unit.php';
}
