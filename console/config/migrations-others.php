<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
    '@backend/migrations',
//    '@bedezign/yii2/audit/migrations',
    '@cornernote/workflow/manager/migrations',
    '@mdm/admin/migrations',
    '@mdm/autonumber/migrations',
    '@mdm/upload/migrations',
    '@vendor/lajax/yii2-translate-manager/migrations',
    '@vendor/elitedivision/yii2-filemanager/migrations',
    '@vendor/elitedivision/amos/audit/migrations',
    '@yii/rbac/migrations',
];