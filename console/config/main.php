<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */
use yii\helpers\ArrayHelper;

$common = require(__DIR__ . '/../../common/config/main.php');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$modules = ArrayHelper::merge(
    $common['modules'],
    require(__DIR__ . '/modules-others.php'),
    require(__DIR__ . '/modules-amos.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO verificare che non ci siano index che non caricano bootstrap, in caso non ce ne siano, questa va eliminata
$bootstrap = ArrayHelper::merge(
    $common['bootstrap'],
    require(__DIR__ . '/bootstrap.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$components = ArrayHelper::merge(
    $common['components'],
    require(__DIR__ . '/components-others.php'),
    require(__DIR__ . '/components-amos.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$params = ArrayHelper::merge(
    $common['params'],
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'elitedivision\amos\core\migration\MigrateController',
            'migrationLookup' => ArrayHelper::merge(
                require(__DIR__ . '/migrations-amos.php'),
                require(__DIR__ . '/migrations-others.php')
            ),
        ],
        'language' => [
            'class' => 'elitedivision\amos\core\commands\LanguageSourceController',
        ],
        'userutility' => [
            'class' => 'elitedivision\amos\admin\commands\UserUtilityController',
        ],
        'utility' => [
            'class' => 'elitedivision\amos\utility\commands\UtilityController',
        ],
    ],
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    'aliases' => [
        '@web' => '',
    ],
    'bootstrap' => $bootstrap,
    'components' => $components,
    'modules' => $modules,
    'params' => $params,
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
];
