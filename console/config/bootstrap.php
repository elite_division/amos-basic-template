<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

$bootstrap = [];

$bootstrap[] = 'log';
$bootstrap[] = 'notify';

return $bootstrap;
