<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return[
    '@vendor/elitedivision/amos-admin/src/migrations',
    '@vendor/elitedivision/amos-attachments/src/migrations',
    '@vendor/elitedivision/amos-chat/src/migrations',
    '@vendor/elitedivision/amos-comments/src/migrations',
    '@vendor/elitedivision/amos-community/src/migrations',
    '@vendor/elitedivision/amos-comuni/src/migrations',
    '@vendor/elitedivision/amos-core/migrations',
    '@vendor/elitedivision/amos-cwh/src/migrations',
    '@vendor/elitedivision/amos-dashboard/src/migrations',
    '@vendor/elitedivision/amos-discussioni/src/migrations',
    '@vendor/elitedivision/amos-documenti/src/migrations',
    '@vendor/elitedivision/amos-email-manager/src/migrations',
    '@vendor/elitedivision/amos-my-activities/src/migrations',
    '@vendor/elitedivision/amos-news/src/migrations',
    '@vendor/elitedivision/amos-notify/src/migrations',
    '@vendor/elitedivision/amos-privileges/src/migrations',
    '@vendor/elitedivision/amos-report/src/migrations',
    '@vendor/elitedivision/amos-tag/src/migrations',
    '@vendor/elitedivision/amos-upload/src/migrations',
    '@vendor/elitedivision/amos-utility/src/migrations',
    '@vendor/elitedivision/amos-workflow/src/migrations',
    /****************DO NOT REMOVE****************/
];