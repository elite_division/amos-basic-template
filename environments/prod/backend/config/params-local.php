<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

$googleMapsApiKey = 'set_your_api_key';

$params = [
    'google-maps' => [
        'key' => $googleMapsApiKey
    ],
    'googleMapsApiKey'=> $googleMapsApiKey,
    'googleMapsLanguage' =>'it',
];

return $params;
