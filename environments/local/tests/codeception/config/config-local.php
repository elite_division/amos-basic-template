<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=amos_local',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
    ],
];
