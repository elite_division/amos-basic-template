<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
    'components' => [
        'urlManager' => [
            'baseUrl' => '/',
            'hostInfo' => 'SITE_URL_HERE',
        ],
    ],
];
