<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

/*---------- begin: BOOTSTRAP --------*/
$bootstrap[] = 'comments';
/*---------- end: BOOTSTRAP --------*/

/*---------- begin: COMPONENTS AMOS --------*/
/*---------- end: COMPONENTS AMOS --------*/

/*---------- begin: COMPONENTS OTHERS --------*/
$components = [
    'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => '',
    ],
];
/*---------- end: COMPONENTS OTHERS --------*/

/*---------- begin: MODULES AMOS --------*/
$modules['chat'] = [
    'class' => 'elitedivision\amos\chat\AmosChat',
];
$modules['comments'] = [
    'class' => 'elitedivision\amos\comments\AmosComments',
    'enableMailsNotification' => false,
    'modelsEnabled' => [
        'elitedivision\amos\discussioni\models\DiscussioniTopic',
        'elitedivision\amos\documenti\models\Documenti',
        'elitedivision\amos\events\models\Event',
        'elitedivision\amos\news\models\News',
    ],
];
$modules['cwh'] = [
    'cached' => false,
];
$modules['dashboard'] = [
        'useWidgetGraphicDashboardVisible' => true,
        'useWidgetGraphicOrder' => true,
];
$modules['discussioni'] = [
    'class' => 'elitedivision\amos\discussioni\AmosDiscussioni',
];
$modules['documenti'] = [
    'class' => 'elitedivision\amos\documenti\AmosDocumenti',
    'params' => [
        'site_publish_enabled' => true,
        'site_featured_enabled' => true
    ],
];
$modules['events'] = [
    'class' => 'elitedivision\amos\events\AmosEvents',
];
$modules['faq'] = [
    'class' => 'elitedivision\amos\faq\AmosFaq',
];
$modules['favorites'] = [
    'class' => 'elitedivision\amos\favorites\AmosFavorites',
    'modelsEnabled' => [
        'elitedivision\amos\discussioni\models\DiscussioniTopic',
        'elitedivision\amos\documenti\models\Documenti',
        'elitedivision\amos\news\models\News',
    ]
];
$modules['inforeq'] = [
    'class' => 'elitedivision\amos\inforeq\AmosInforeq',
];
$modules['news'] = [
    'class' => 'elitedivision\amos\news\AmosNews',
    'params' => [
        'site_publish_enabled' => true,
        'site_featured_enabled' => true
    ]
];
$modules['privileges'] = [
    'class' => 'elitedivision\amos\privileges\AmosPrivileges',
];
$modules['report'] = [
    'class' => 'elitedivision\amos\report\AmosReport',
    'modelsEnabled' => [
        'elitedivision\amos\discussioni\models\DiscussioniTopic',
        'elitedivision\amos\documenti\models\Documenti',
        'elitedivision\amos\events\models\Event',
        'elitedivision\amos\news\models\News',
    ],
];
if (isset($modules['tag'])) {
    if (isset($modules['community'])) {
        $modules['tag']['modelsEnabled'][] = 'common\models\Community';
    }
}
/*---------- end: MODULES AMOS --------*/

/*---------- begin: MODULES OTHERS --------*/
/*---------- end: MODULES OTHERS --------*/

$config = [
    'name' => 'Amos Basic Template DEMO',
    'bootstrap' => $bootstrap,
    'components' => $components,
    'modules' => $modules,
];

return $config;
