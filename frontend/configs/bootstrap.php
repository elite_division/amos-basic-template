<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

$bootstrap = [];
$bootstrap[] = 'cms';
$bootstrap[] = 'workflow';


/*$bootstrap[] = [
    'class' => 'elitedivision\amos\core\components\LanguageSelector',
    'supportedLanguages' => ['en-GB', 'it-IT'],
    'allowedIPs' => ['*']
];*/

return $bootstrap;
