<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */

return [
    'workflowSource' => [
        'class' => 'elitedivision\amos\core\workflow\ContentDefaultWorkflowDbSource',
    ],
    
];