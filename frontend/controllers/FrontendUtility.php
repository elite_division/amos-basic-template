<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace frontend\controllers;

use elitedivision\amos\comuni\models\IstatProvince;
use yii\helpers\ArrayHelper;

class FrontendUtility
{

    public static function getIstatProvince()
    {
        return ArrayHelper::map(IstatProvince::find()->orderBy('nome')->asArray()->all(), 'id', 'nome');
    }
}