<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

return [
    'kickstarter_success' => '<h1 class="alert-heading">Вы сделали это!</h1>
    <p>Да, вы успешно установили <i>LUYA</i>. Теперь вы можете войти в административную панель для создания страниц.</p>',
    'kickstarter_admin_link' => '<p><a href="{link}" class="btn btn-primary btn-lg">Перейти в административную панель</a></p>',
];
