<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

return [
    'stripe_header_title' => 'Payment Checkout',
    'stripe_item_list_title' => 'Dettagli',
    'stripe_items_total' => 'Totale',
    'stripe_card_info' => 'Pagamento con carta di credito/debito',
    'stripe_button_abort' => 'Annulla',
    'stripe_button_pay' => 'Paga',
];
