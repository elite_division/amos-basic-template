<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    elitedivision\amos\basic\template
 * @category   CategoryName
 */
$googleMapsApiKey = 'AIzaSyAFc8ESM0Rv33ruzSLTaVEPwxTfzCRcU94';

return [
    'supportEmail' => 'helpfrontend@example.com',
    'versione' => '1.2.16', // Version
    'user.passwordResetTokenExpire' => 86400,
    'google-maps' => [
        'key' => $googleMapsApiKey
    ],
];
