<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace app\modules\cmsapi\frontend\models;

use app\modules\cmsapi\frontend\utility\CmsObject;


class CmsResultCreatePage extends CmsObject
{
    public $nav_id;
    public $nav_id_tks_page;
    public $nav_id_wating_page;
    public $nav_id_already_present_page;
    public $preview_url;

}