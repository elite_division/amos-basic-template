<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace  app\modules\uikit\admin;

use Yii;
use luya\base\CoreModuleInterface;


final class Module extends \luya\admin\base\Module implements CoreModuleInterface
{
    /**
     * @inheritdoc
     */
    public function getAdminAssets()
    {
        return  [
            'app\modules\uikit\assets\MainAsset',
        ];
    }
}
