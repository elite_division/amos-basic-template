<!DOCTYPE html>
<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

use app\assets\SocialAsset;
use elitedivision\amos\core\forms\ActiveForm;
use elitedivision\amos\core\helpers\Html;

SocialAsset::register($this);

?>
<div>
    <?php
    //Uikit::trace($data);
    //Uikit::trace($debug);
    //Uikit::trace($request);
    //Uikit::trace($model->attributes());
    $form            = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => "off",'target'=>'_blank'],
            'encodeErrorSummary' => false,
            'fieldConfig' => ['errorOptions' => ['encode' => false, 'class' => 'help-block']]
    ]);
    ?>


    <div class="uk-form-controls">
        <div>
            <?php
                echo Html::hiddenInput('id', $data['id']);
                echo !empty($data['description']) ? $data['description'] : 'Export';
            ?>
        </div>
        <?=
        Html::submitButton(!empty($data['submitlabel']) ? $data['submitlabel'] : 'Submit',
            ['class' => 'btn btn-primary'])
        ?>
    </div>
</div>
<?php
ActiveForm::end();
?>
</div>
