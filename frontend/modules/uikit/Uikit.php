<?php

/**
 * Aria S.p.A.
 * OPEN 2.0
 *
 *
 * @package    Open20Package
 * @category   CategoryName
 */

namespace app\modules\uikit;


/**
 * Description of Uikit
 *
 * @author stefano.cavazzini
 */
class Uikit extends \trk\uikit\Uikit{
  
    public static function pickBool($array, $keys)
    {
        $retarray = array_intersect_key($array, array_flip((array) $keys));
        foreach($retarray as $key => $value)
        {
            if(empty($retarray[$key])){
                $retarray[$key] = "false";
            }
        }
        return $retarray;
    }
}
